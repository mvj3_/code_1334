 利用UITableView 显示多级树形目录。可以用于文件查看系统。

    借助以下两个属性实现多级树形目录

    @property(nonatomic) NSInteger indentationLevel; // adjust content indent. default is 0

    @property(nonatomic) CGFloat indentationWidth; // width for each level. default is 10.0
